#include <stdio.h>
#include <string.h>

#define MAXELE 30
#define DIM 40
#define MENU 0
#define INS 1
#define CER 2
#define CAN 3
#define VIS 4
#define OUT 5

/* Definizione delle struttura persona */
struct persona
{ char nome[DIM];
  char cognome[DIM];
  char indirizzo[DIM];
  int eta;
};

/* vettore di persona con dimensione MAXELE */
persona anag[MAXELE];

/* indice del vettore persona inizializzato a zero */
int index = 0;

int menu_persona(void);
int ins_persona(int);
struct persona * cer_persona(char *, char *, char *, int);
void can_persona(void);
void eli_persona(struct persona *);
void ric_persona(void);
void vis_persona(struct persona *);
void vis_anag(void);

/* Presentazione del menù e scelta della funzione */

void main(void)

{
    int scelta = MENU;
    while (scelta!=OUT) {
        switch(scelta) {
            case MENU:
            scelta = menu_persona();
            if (scelta==MENU)
            scelta = OUT;
            break;
            case INS:
            index = ins_persona(index);
            scelta = MENU;
            break;
            case CAN:
            can_persona();
            scelta = MENU;
            break;
            case CER:
            ric_persona();
            scelta = MENU;
            break;
            case VIS:
            vis_anag();
            scelta = MENU;
            break;
        }
    }
}

/* Definizione formale della funzione Menù */

int menu_persona(void)
{
    int scelta;
    char invio; 
    int true = 1;
    while(true)
    { printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
      printf("\t\t\t ANAGRAFE");
      printf("\n\n\t\t\t 1.INSERISCI PERSONA");
      printf("\n\n\t\t\t 2.CANCELLA PERSONA");
      printf("\n\n\t\t\t 3.RICERCA PERSONA");
      printf("\n\n\t\t\t 4.VISUALIZZA ANAGRAFE");
      printf("\n\n\t\t\t 5.ESCI");
      printf("\n\n\n\t\t\t Quale opzione vuoi scegliere?");
      scanf("%d", &scelta);
      scanf("%c", &invio);
      printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
      switch(scelta) {
          case 0:
          case 1:
          case 2:
          case 3:
          case 4:
            return(scelta);
            default: 
            break;
      }
}
return(0);
}

/* Funzione di inserimento persona nell'anagrafe */
int ins_persona(int pos)
{
    
}