/*Scrivere le strutture dati necessarie p er la rappresentazione di una lista di candidati p er un concorso da profess ore universitario
[6 punti]. Per ogni candidato si deve p oter memorizzare il nome, il cognome, la data e il luogo di nascita, il co dice fiscale.
Inoltre, a ogni candidato asso ciata una lista di pubblicazioni. Per ogni pubblicazione si deve p oter memorizzare il titolo, gli
autori (massimo 5), il nome della rivista e l’anno di pubblic azione. Si realizz i una funzione che p ermetta di stampare a video
i dati di tutte le pubblicazioni di un candidato (p er identificare il candidato la funzione riceve in ingresso il co dic e fiscale)
[6 punti]. Si realizzi inoltre una funzione che p ermetta di cancellare tutti i candidati che hanno un numero di pubblicazioni
inferiore a 10 [6 punti].*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define n_max 30

typedef char stringa[n_max];

typedef enum{GEN=1,FEB,MAR,APR,MAG,GIU,LUG,AGO,SET,OTT,NOV,DIC} TipoMese;

typedef struct{
    unsigned int giorno;
    TipoMese mese;
    unsigned int anno;
}TipoData;

typedef struct{
    stringa nome;
    stringa cognome;
}TipoNome;

typedef struct{
    stringa titolo;
    TipoNome autore[5];
    stringa rivista;
    unsigned int anno_pub;
    int num_autori; //numero di pubblicazioni per ogni autore
}TipoPubblicazione;

typedef struct NodoPubblicazione{
    TipoPubblicazione info;
    struct NodoPubblicazione* next;
}TipoNodoPubblicazione;

typedef TipoNodoPubblicazione* TipoListaPubblicazioni;

typedef struct{
    TipoNome nominativo;
    TipoData data_birth;
    stringa luogo_birth;
    stringa codice_fiscale;
    TipoListaPubblicazioni lista_pubblicazioni;
}TipoCandidato;

typedef struct NodoCandidato{
    TipoCandidato info;
    struct NodoCandidato* next;
}TipoNodoCandidato;

typedef TipoNodoCandidato* TipoListaCandidati;

//Stampa delle pubblicazioni di ciascun candidato.

void StampaPubblicazioni(TipoListaCandidati* lista, stringa codice_fiscale) //TipoListaCandidati serve a scorrere la lista per ogni candidato
{
    TipoNodoPubblicazione* curr_pub = NULL;
    TipoNodoCandidato* curr = lista; //lista in questo momento non ha contenuto
    while(curr!=NULL && strcmp(curr->info.codice_fiscale,codice_fiscale)!=0 )
    {curr=curr->next; //collego nodo precedente al nodo successivo
    }
    curr_pub = curr->lista_pubblicazioni; //i due nodi si eguagliano e puntano entrambi ad un attributo di TipoCandidato(lista_pubblicazioni)
    while(curr!=NULL){
    int i=0;
    for (i=0; i<curr_pub->num_autori; i++) {
        printf("-->%s%s",curr_pub->autore[i].nome,curr_pub->autore[i].cognome);
    }
        printf("-->%s%s%d",curr_pub->titolo,curr_pub->rivista,curr_pub->anno_pub);
        curr_pub=curr_pub->next;
    }
}

//Conta pubblicazioni del candidato.

int ContaPubblicazioni(TipoNodoCandidato* candidato) //num_autori posso ometterlo come parametro di ingresso perché fa parte della struct TipoCandidato
{
    int cont=0;
    TipoNodoCandidato* curr_pub = candidato->info.lista_pubblicazioni; //valore corrente deve essere uguale al candidato puntato al campo lista_pubblicazioni di info
    while(curr_pub!=NULL)
    {cont++;
    curr_pub=curr_pub->next;}
    return cont;
}

//Dealloca

void DeallocaCandidato(TipoNodoCandidato* candidato) //Vogliamo cancellare ogni nodo associato ad un candidato.
{
    TipoNodoPubblicazione* curr = candidato->info.lista_pubblicazioni; //candidato->info conterrà gli attributi della lista_pubblicazioni
    while(curr!=NULL)
    {
        temp=curr; //associo ad una variabile temporanea il nodo corrente.
        curr=curr->next;
        free(temp);
    }
    free(candidato);
}

//Cancellazione dei candidati con num_pubblicazioni inferiore a 10. 

void CancellaCandidati(TipoListaCandidati* indirizzo_testa)
{
    TipoNodoCandidato* curr = *indirizzo_testa;
    TipoNodoCandidato* prec = NULL;
    while(ContaPubblicazioni(curr)<10){
        *indirizzo_testa = *indirizzo_testa->next;
        DeallocaCandidato(curr);
        curr = *indirizzo_testa;
    }
        prec=curr;
        curr=curr->next;
    while(curr!=NULL){ //così consideriamo anche il caso in cui curr sia uguale a NULL, perché l'avevamo inizializzato.
        if(ContaPubblicazioni(curr)<10){
            prec->next = curr->next;
            DeallocaCandidato(curr);
            curr = prec->next;
    }
    curr = curr->next;
}
}