/*Si scrivano le strutture dati necessarie per rappresentare una lista di misurazioni effettuate da una centralina per la rilevazione
degli inquinanti PM10 e PM2.5. Per ogni misurazione si vogliono memorizzare la data e l’orario in cui è stata effettuata e i
livelli di PM10 e PM2.5. [3 punti]
Si realizzi una funzione che permetta di cancellare tutte le misurazioni contenute nella lista. [3 punti]
Si realizzi inoltre una funzione che, qualora il numero di misurazioni sia superiore a 100, effettui il salvataggio della lista
sul file “misurazioni.txt” appendendole in coda al file e poi proceda alla cancellazione delle misurazioni contenute nella lista.
[4 punti]*/

typedef enum{GEN=1,FEB,MAR,APR,MAG,GIU,LUG,AGO,SET,OTT,NOV,DIC} TipoMese;

typedef struct{
unsigned int giorno;
TipoMese mese;
unsigned int anno;
}TipoData;

typedef struct{
unsigned int secondi;
unsigned int minuti;
unsigned int ore;
}TipoOrario;

typedef struct{
TipoData data;
TipoOrario orario;
float PM10;
float PM25;
}TipoInfo;

typedef struct{
TipoInfo info;
struct nodo* next;
}TipoNodo;

typedef TipoNodo* TipoLista;

void CancellaInTesta(TipoLista* indirizzo_testa)
{
    if(*indirizzo_testa!=NULL)
    {
        TipoNodo* temp=(*indirizzo_testa)->next;
        free(*indirizzo_testa);
        *indirizzo_testa=temp;
    }
}

//Cancellazione di tutte le misurazioni contenute nella lista:
void CancellaTutto(TipoLista* indirizzo_testa)
{
    while(*indirizzo_testa!=NULL)
    {
        CancellaInTesta(indirizzo_testa);
    }
}

int ContaMisurazioni(TipoNodo* misurazione)
{
    int cont=0;
    TipoNodo* curr=misurazione->info.data, misurazione.info.orario, misurazione.info.PM10, misurazione.info.P25; //oppure misurazione->info.Info
    while(curr!=NULL)
    {
        curr=curr->next;
        cont++;
    }
    return cont;
}

/*Si realizzi inoltre una funzione che, qualora il numero di misurazioni sia superiore a 100, effettui il salvataggio della lista
sul file “misurazioni.txt” appendendole in coda al file e poi proceda alla cancellazione delle misurazioni contenute nella lista.*/
void SalvaSuFile(TipoLista testa)
{
    TipoNodo* curr=testa;
    char nomefile[30];
    FILE* f=NULL;
    while(curr!=NULL) {
        if (ContaMisurazioni(curr)>100)
        {
            f=fopen("misurazioni.txt","a");
            fprintf(f,"%d%d%d%d%d%d%f%f",curr->info.data.giorno,curr->info.data.mese,curr->info.data.anno,curr->info.orario.secondi,curr->info.orario.minuti,
            curr->info.orario.ore,curr->info.PM10,curr->info.PM25);
            fclose(f);
        }
        curr=curr->next;
    }
}
