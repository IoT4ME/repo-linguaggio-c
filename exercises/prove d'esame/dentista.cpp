/*Si scrivano le strutture dati necessarie per rappresentare una lista di appuntamenti di uno studio dentistico.
Per ogni appuntamento si vogliono memorizzare il nome del paziente, la data e l’orario in cui si terrà l’appuntamento e il tipo di intervento
(VISITA, IGIENE, OTTURAZIONE, ESTRAZIONE, IMPIANTO, ORTODONZIA). Si realizzi una funzione che cancelli tutti
gli appuntamenti antecedenti a una certa data (passata come parametro alla funzione). [6 punti] Si realizzi inoltre una funzione
che permetta di salvare gli appuntamenti in file diversi a seconda del tipo di intervento. [6 punti]*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define n_max 20
#define VISITA 1
#define IGIENE 2

typedef enum {GEN=1,FEB,MAR,APR,MAG,GIU,LUG,AGO,SET,OTT,NOV,DIC} TipoMese;

typedef enum {VISITA, IGIENE, OTTURAZIONE, ESTRAZIONE, IMPIANTO, ORTODONZIA} TipoIntervento;

typedef struct{
unsigned int ora;
unsigned int minuti;
} TipoOrario;

typedef struct{
unsigned int giorno;
TipoMese mese;
unsigned int anno;
} TipoData;

typedef struct{
char nome[n_max];
TipoData data;
TipoOrario orario;
TipoIntervento intervento;
} TipoInfo;

typedef struct nodo{
TipoInfo info;
struct nodo* next;
} TipoNodo;

typedef struct TipoNodo* TipoLista;

void CancellaInTesta(TipoLista* indirizzo_testa)
{
    if(*indirizzo_testa!=NULL)
    {
        TipoNodo* temp = (*indirizzo_testa)->next;
        free(*indirizzo_testa);
        *indirizzo_testa = temp;
    }
}

void CancellaElementi(TipoLista* indirizzo_testa, TipoData dt)
{
    TipoNodo* curr;
    TipoNodo* prec;
    while(*indirizzo_testa!=NULL && (*indirizzo_testa)->data==dt)
    {
        CancellaInTesta(indirizzo_testa);
    }
    if((*indirizzo_testa!=NULL) && ((curr->info.data.anno<dt.anno) || (curr->info.data.anno==dt.anno && curr->info.data.mese<dt.mese) || (curr->info.data.anno==dt.anno && curr->info.data.mese==dt.mese && curr->info.data.giorno<dt.giorno))) //potevo scrivere curr->info.data<dt;
    {
        TipoNodo* curr = (*indirizzo_testa)->next; //curr non si crea con la malloc perchè è un puntatore.
        TipoNodo* prec = NULL;
        while(curr!=NULL){
            if((curr->info.data.anno<dt.anno) || (curr->info.data.anno==dt.anno && curr->info.data.mese<dt.mese) || (curr->info.data.anno==dt.anno && curr->info.data.mese==dt.mese && curr->info.data.giorno<dt.giorno))
            {
                prec->next=curr->next;
                free(curr);
                curr=prec;
            }
            prec=curr;
            curr=curr->next;
        }
    }
}
void SalvaAppuntamenti(TipoLista testa, Tipoinfo intervento)
{
    TipoNodo* curr=testa;
    int scelta;
    char stringa[30];
    FILE* f = NULL;
    while(curr!=NULL){
    switch(curr->info.intervento)
    {
        case 1: 
        f=fopen("visita.txt","a");
        fprintf(f,"%s%d%d%d%d%d", curr->info.nome, curr->info.data.anno, curr->info.data.mese, curr->info.data.giorno, curr->info.orario.ora, curr->info.orario.minuti);
        fclose(f);
        break;
        case 2:
        f=fopen("igiene.txt","a");
        fprintf(f,"%s%d%d%d%d%d", curr->info.nome, curr->info.data.anno, curr->info.data.mese, curr->info.data.giorno, curr->info.orario.ora, curr->info.orario.minuti);
        fclose(f);
        break;
        case OTTURAZIONE: strcpy(nomefile,"otturazione.txt");
        break;
        case ESTRAZIONE: strcpy(nomefile,"estrazione.txt");
        break;
        case IMPIANTO: strcpy(nomefile,"impianto.txt");
        break;
        case ORTODONZIA: strcpy(nomefile,"ortodonzia.txt");
        break;
    default:    printf("Inserisci il tipo di intervento: ");
                scanf("%d",&scelta); }
    }
    curr=curr->next;
}