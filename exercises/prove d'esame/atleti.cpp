/*Scrivere le strutture dati necessarie per la rappresentazione di una lista di atleti partecipanti a una gara di sci [6 punti]. Per
ogni atleta si deve poter memorizzare il nome, il cognome, la nazionalità, il numero di pettorale, il tempo della prima manche
e il tempo della seconda manche. I tempi vengono espressi in secondi e millisecondi (in caso di caduta o squalifica i tempi
vengono messi a zero). Si realizzi una funzione che permetta di cancellare tutti gli utenti che non hanno terminato entrambe
le manche [6 punti]. Si realizzi inoltre una funzione che riordini la lista in ordine decrescente dei tempi complessivi (somma
dei tempi delle due manche) [6 punti].*/

typedef char stringa[30];

typedef struct{
float secondi;
float millisecondi;
}TipoTempo;

typedef struct{
stringa nome;
stringa cognome;
stringa nazionalità;
int pettorale;
TipoTempo manche1;
TipoTempo manche2;
}TipoInfo;

typedef struct nodo{
TipoInfo info;
struct nodo* next;
}TipoNodo;

typedef TipoNodo* TipoLista;

void CancellaInTesta(TipoLista* indirizzo_testa)
{
    if(*indirizzo_testa!=NULL)
    {
        TipoNodo* temp=(*indirizzo_testa)->next;
        free(*indirizzo_testa);
        *indirizzo_testa=temp;
    }
}

//Cancellazione di tutti gli atleti che non hanno terminato entrambe le manche:
void CancellaAtleti(TipoLista* indirizzo_testa, TipoInfo manche)
{
    while(*indirizzo_testa!=NULL)
    {
        if(*indirizzo_testa)->info.manche1==0 || (*indirizzo_testa)->info.manche2==0)
        {
            CancellaInTesta(indirizzo_testa);
        }
    }
    if(*indirizzo_testa!=NULL){
        TipoNodo* curr=(*indirizzo_testa)->next;
        TipoNodo* prec=NULL;
        while(curr!=NULL){
            if(curr->info.manche1==0 || curr->info.manche2==0){
                prec->next=curr->next;
                free(curr);
                curr=prec;
            }
            prec=curr;
            curr=curr->next;
        }
    }
}

