//Definire una struttura di studenti(ognuno con nome, cognome e numero di matricola) di un corso universitario. Di ognuno occorre memorizzare un voto
//accettabile. Stampare a video il voto più alto.

#include <stdio.h>

#define iscritti 165
#define voto_min 18

struct studente {
char nome[20], cognome[20], matricola[10];
int voto;
};

void main(void)
{

int i;

struct studente ambientale[iscritti];

    for (i=0; i<iscritti; i++) {
        printf("Inserisci il nome dello studente: ");
        scanf("%s",&ambientale[i].nome);
        printf("Inserisci il cognome dello studente: ");
        scanf("%s",&ambientale[i].cognome);
        printf("Inserisci la matricola dello studente: ");
        scanf("%s",&ambientale[i].matricola);
        printf("Inserisci il voto conseguito: ");
        scanf("%d",&ambientale[i].voto);
        }

printf("Elenco degli ammessi alla verbalizzazione \n");

    for (i=0; i<iscritti; i++)  {
    if(ambientale[i].voto>=voto_min)
    printf("%s %s %s %d \n", ambientale[i].nome, ambientale[i].cognome,ambientale[i].matricola,ambientale[i].voto);
    }
}
