#include <stdio.h>
#define dim_max 10
void main(void)
{
    int i,N,MIN,MAX,V[dim_max];

        do
    {
        /* inserisco la dimensione del vettore N */
        printf("Inserisci la dimensione del vettore N: ");
        scanf("%d",&N);
    }
    while (N<2 || N>dim_max);
    /* inserisco l'elemento V[0] */
    printf("V[0]=");
    scanf("%d", &V[0]);
    MIN=MAX=V[0];
    i=1;
    do
    {
        /* inserisco tutti gli elementi dopo V[0] ma fino alla dimensione V[N] */
        printf("V[%d]=", i);
        scanf("%d", &V[i]);
        if (V[i] < MIN)
                MIN = V[i];
        else 
                if (V[i] > MAX)
                    MAX = V[i];
        i = i+1;  
    }
    while (i<N);
    printf("Il minimo vale: %d\n", MIN);
    printf("Il massimo vale: %d\n", MAX);
    
}