//Calcolare la media in una gara di 2 prove.

#include <stdio.h>

#define MAX_CONC 50
#define VOTO_MIN 18
#define VOTO_MAX 30

main ()
{
float prova1[MAX_CONC], prova2[MAX_CONC], media[MAX_CONC];
int i, n;

do  {
printf("Quanti concorrenti vuoi visualizzare?: ");
scanf("%d", &n);
    }
while(n<1 || n>MAX_CONC);

for (i=0; i<n; i++) {
    printf("Per il concorrente n.%d", i+1);
do  {
    printf("Voto prima prova: ");
    scanf("%f", &prova1[i]);

    }
while(prova1[i]<VOTO_MIN || prova1[i]>VOTO_MAX);

do  {
    printf("Voto seconda prova: ");
    scanf("%f", &prova2[i]);
    }
while(prova2[i]<VOTO_MIN || prova2[i]>VOTO_MAX);
}

for(i=0; i<n; i++)   {
        
    media[i]=(prova1[i]+prova2[i])/2;

    printf("------------CLASSIFICA--------------\n");


    printf("%f %f %f \n", prova1[i], prova2[i], media[i]);
}
}