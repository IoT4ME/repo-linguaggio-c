#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

struct elemento {
    int inf;
    struct elemento *pun;
};

void gestione_lista(void);
struct elemento *inserisci(struct elemento *);
struct elemento *elimina(struct elemento *);
void visualizzazione(struct elemento *);

main()
{
    gestione_lista();
}

void gestione_lista(void)
{
    struct elemento *punt_lista = NULL; //punt_lista struttura di appoggio alla lista
    int scelta = -1;
    char pausa;
    
while (scelta!=0) {
    printf("\t\t\tGestione di una sequenza di dati per una lista\n");
    printf("Premi 1 per inserire un elemento;\nPremi 2 per cancellare un elemento;\nPremi 3 per visualizzare la lista;\nPremi 0 per uscire dal programma.");
    scanf("%d",&scelta);
    switch(scelta)
    {
    case 1: punt_lista = inserisci(punt_lista);
    break;
    case 2: punt_lista = elimina(punt_lista);
    break;
    case 3: visualizzazione(punt_lista);
    printf("Premi un tasto per continuare.....\n");
    scanf("%c%c", &pausa, &pausa);
    break;
    }
}
}

/* Funzione inserimento nella lista */
struct elemento *inserisci(struct elemento *p)
{
    struct elemento *p0, *p1;
    int posizione;
    p0 = (struct elemento*)malloc(sizeof(struct elemento));
    
    
    printf("Inserisci un numero intero: ");
    scanf("%d",&p0->inf);
    
    if (p==NULL) {
        p=p0;     /* Se la lista è vuota, crea il primo elemento */
        p->pun = NULL;
    }
    else
    if (p->inf>p0->inf) {
        p0->pun=p;
        p=p0;
    }
    else {
    p1=p;
    posizione = 0;
    while(p1->pun!=NULL && posizione !=1) {
        if (p1->pun->inf < p0->inf)
        p1 = p1->pun;
        else 
        posizione = 1;
    }
    p0->pun = p1->pun; /* connessione all'elemento successivo */
    p1->pun = p0;      /* connessione all'elemento precedente */
    }
    }
return (p); /* Ritorno del puntatore all'inizio della lista */
}

/* Funzione elemina dalla lista */
struct elemento *elimina(struct elemento *p)
{
    struct elemento eliminato;
    p = *p1, *p2;
    int posizione = 0;
    char pausa;
    
    printf("Inserisci elemento da eliminare:");
    scanf("%d", &eliminato.inf); //eliminato.inf perchè eliminato è un elemento della struct
    
    if(p1!=NULL) 
    { if(p1->pun==eliminato.inf)
    { p2=p1;
      p=p->pun;
      free(p2);
      return p;
    }
    }
    else {
        while(p1->pun!=NULL && posizione !=1) { 
            if(p1->pun->inf!=eliminato.inf)
        p1=p1->pun;
        else {
        posizione = 1;
        p2=p1->pun;
        p1->pun=p1->pun->pun;
        free(p2);
        return(p);
    }
}
}
if(!posizione)
{ printf("Elemento non trovato!\n");
scanf("%c%c", &pausa, &pausa);
}
return(p);    
}

void visualizzazione(struct elemento *p)
{
    struct elemento *output = p;
    printf("\t\t\t STAMPA LISTA\n");
    if(output==NULL)
        printf("NULL");
    else
    do {
        printf("--> %d", output->inf);
        output = output->pun;
    } while(output!=NULL);
}
