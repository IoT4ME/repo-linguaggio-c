#include <stdio.h>

#define M 2
#define N 3
#define Q 2

int matA [M][N];        /* definizione prima matrice */
int matB [N][Q];        /* definizione seconda matrice */
int matC [M][Q];        /* definizione prodotto prima e seconda matrice */

main ()
{
    int i, j, k;
    printf("\n \n Caricamento elementi della prima matrice: \n \n");
    for(i=0; i<M; i++)
        for(j=0; j<N; j++)
        { printf("Inserisci la riga i-esima %d e la colonna j-esima %d:", i, j);
          scanf("%d", &matA[i][j]);
        };
        
    printf("\n \n Caricamento elementi della seconda matrice: \n \n");
    for(i=0; i<N; i++)
        for(j=0; j<Q; j++)
        { printf("Inserisci la riga i-esima %d e la colonna j-esima %d:", i, j);
          scanf("%d", &matB[i][j]);
        };

for(i=0; i<M; i++)
    for(j=0; j<Q; j++)  { 
    matC[i][j] = 0;
for(k=0; k<N; k++)
matC[i][j] = matC[i][j] + matA[i][k] * matB[k][j];
};

printf("\n \n Mostra la matrice A \n");
for(i=0; i<M; i++)  {
printf("\n");
        for(j=0; j<N; j++)
        printf("%5d", matA[i][j]);
}

printf("\n \n Mostra la matrice B \n");
for(i=0; i<N; i++)  {
printf("\n");
        for(j=0; j<Q; j++)
        printf("%5d", matB[i][j]);
}


printf("\n \n Visualizza la matrice prodotto C \n");
for(i=0; i<M; i++)  {
printf("\n");
        for(j=0; j<Q; j++)
        printf("%5d", matC[i][j]);
}
}
